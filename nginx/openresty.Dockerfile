FROM openresty/openresty:1.25.3.1-0-alpine

RUN apk add --no-cache --virtual .build-deps \
  gcc \
  libc-dev \
  make \
  openssl-dev \
  pcre-dev \
  zlib-dev \
  linux-headers \
  curl \
  gnupg \
  libxslt-dev \
  gd-dev \
  geoip-dev \
  sudo

RUN wget https://luarocks.org/releases/luarocks-3.9.2.tar.gz && \
    tar zxpf luarocks-3.9.2.tar.gz && \
    cd luarocks-3.9.2 && \
    ./configure && make && sudo make install

RUN sed -i '/WGET/d' /usr/local/share/lua/5.1/luarocks/fs/tools.lua
RUN cat /usr/local/share/lua/5.1/luarocks/fs/tools.lua
RUN sudo luarocks install lua-resty-openssl
