# Caddy GitHub Webhook

# Sources

- [Caddy Server](https://caddyserver.com)
- [Caddy HMAC module](https://github.com/abiosoft/caddy-hmac)
- [Custom module build example](https://hub.docker.com/_/caddy)
- [Caddy with Docker Compose example](https://caddyserver.com/docs/running#docker-compose)
- [ngrok with Docker Compose example](https://ngrok.com/docs/using-ngrok-with/docker/)

# Build Caddy with Custom Module

[Dockerfile](./Dockerfile)
```Dockerfile
FROM caddy:2.7.2-builder AS builder

RUN xcaddy build \
    --with github.com/abiosoft/caddy-hmac

FROM caddy:2.7.2

COPY --from=builder /usr/bin/caddy /usr/bin/caddy
```

# Configure Caddy with HMAC

```Caddyfile
@github {
    path /webhook
    header_regexp X-Hub-Signature "[a-z0-9]+\=([a-z0-9]+)"
}
@hmac {
    expression {hmac.signature} == {http.regexp.1}
}
route @github {
    hmac sha1 {$GITHUB_WEBHOOK_SECRET}
    respond @hmac "You're in!"
}
```

# Run Caddy with Docker Compose

See [docker-compose.yml](./docker-compose.yml) and [official docs](https://caddyserver.com/docs/running#docker-compose).


# Expose Caddy with ngrok

In order to test a GitHub webhook we need to expose Caddy to the internet. The safest and cheapest way to do that is with [ngrok](https://ngrok.com). You will require a `NGROK_AUTHTOKEN` [environment variable](https://ngrok.com/docs/secure-tunnels/ngrok-agent/reference/#supported-env-variables) to authenticate with ngrok.

```yml
version: 2
tunnels:
  caddy:
    proto: http
    addr: https://caddy.local
    host_header: rewrite # required, otherwise your Host header will be https://XXXXX.ngrok-free.app
```

# Test the webhook

Set up a GitHub webhook with the following info:

Key          | Value
-------------|------
Payload URL  | https://XXXXX.ngrok-free.app/webhook
Content type | `application/json`
Secret       | `GET_YOUR_OWN`

You should see the ping event in the `Recent Deliveries` tab in `Webhooks/Manage webhook`. You can also look at http://localhost:4040 to see the local ngrok log.

If the secret matches you'll see `You're in!` otherwise you'll see `Hello, world!`.